package com.example.songanext;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.os.Bundle;

public class Bike extends AppCompatActivity {
    private Button bhomebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike);
        bhomebutton = (Button)findViewById(R.id.button2);
        bhomebutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {

                Intent intent = new Intent(Bike.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}