package com.example.songanext;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.os.Bundle;

public class Laptop extends AppCompatActivity {
    private Button lhomebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop);
        lhomebutton = (Button)findViewById(R.id.button1);
        lhomebutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {

                Intent intent = new Intent(Laptop.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}