package com.example.songanext;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;


public class MainActivity extends AppCompatActivity {

    private Button Carbutton;
    private Button laptopbutton;
    private Button bikebutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       Carbutton = (Button)findViewById(R.id.first_activity_button);
        laptopbutton = (Button)findViewById(R.id.second_activity_button);
        bikebutton = (Button)findViewById(R.id.third_activity_button);
        Carbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {
                Intent intent = new Intent(MainActivity.this, Car.class);
                startActivity(intent);
            }
        });
        laptopbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {
                Intent intent = new Intent(MainActivity.this, Laptop.class);
                startActivity(intent);
            }         });
       bikebutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {
                Intent intent = new Intent(MainActivity.this, Bike.class);
                startActivity(intent);
            }
       });
            }
}